(require 'cl)
(require 'org)

(defun nmd/enable-org-ref ()
  "Installs, enables, and configures org-ref."
  (defun nmd/install-org-ref ()
    (require 'package)
    (package-initialize)
    (add-to-list 'package-archives
	         '("melpa" . "http://melpa.org/packages/") t)
    (if (< (string-to-number emacs-version) 26.3)
        (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))
    (package-refresh-contents)
    (package-install 'org-ref)
    (require 'org-ref))

  (condition-case err
      (require 'org-ref)
    (error (nmd/install-org-ref)))

  (define-key org-mode-map (kbd "C-c C-x l") 'org-ref-helm-insert-cite-link)
  (setq org-latex-pdf-process
        '("latexmk -shell-escape -bibtex -f -pdf %f")))
(nmd/enable-org-ref)

(defun nmd/enable-lorikeem ()
  "Add Lorikeem for font lock on Mumps."
  (add-to-list 'load-path "~/lorikeem")
  (require 'mumps-mode)
  (add-to-list 'auto-mode-alist '("\\.m\\'" . mumps-mode))
  (add-to-list 'auto-mode-alist '("\\.mscript\\'" . mumps-mode)))
;; if it's not available, we're building on Gitlab CI.
(ignore-errors (nmd/enable-lorikeem))


(defun nmd/add-tangled-name (backend)
  (let ((src-blocks (org-element-map (org-element-parse-buffer) 'src-block #'identity)))
    (setq src-blocks (nreverse src-blocks))
    (loop for src in src-blocks
          do
          (goto-char (org-element-property :begin src))
          (let ((tangled-name (org-element-property :name src)))
            (if (bound-and-true-p tangled-name)
                (insert (format "
#+begin_export html
<span id=\"codeblock-%s\" />
#+end_export
/<%s>/ =\n" tangled-name tangled-name)))))))

(add-hook 'org-export-before-parsing-hook 'nmd/add-tangled-name)

(defun nmd/org-html-export-to-html-non-interactive ()
  "Export HTML without evaluating code blocks."

  (widen)
  (goto-char (point-min))

  (nmd/org-export-images)

  (let ((org-export-use-babel 'nil))
    (nmd/export-context '(lambda () (org-html-export-to-html)))))

(defun nmd/export-context (execThis)
  "Runs function in correct image export context."

  (require 'graphviz-dot-mode)
  (require 'ob-ditaa)
  (require 'ob-dot)
  (require 'ob-plantuml)
  (require 'ob-python)
  (require 'ob-R)
  (require 'ob-shell)

  (save-excursion
    (goto-char (point-min))
    (let ((org-confirm-babel-evaluate 'nil)
          (org-plantuml-jar-path "/usr/share/plantuml/plantuml.jar")
          (org-ditaa-jar-path "/usr/share/ditaa/ditaa.jar")
          (org-babel-python-command "python3")
          (org-babel-load-languages '((ditaa . t)
                                      (dot . t)
                                      (plantuml . t)
                                      (python . t)
                                      (R . t)
                                      (shell . t))))
      (funcall execThis))))

(defun nmd/org-export-subtree-to-pdf (subtree)
  "Exports named subtree to PDF.

Also exports images separately."
  (widen)
  (org-babel-tangle)
  (nmd/enable-org-ref)
  (goto-char (point-min))

  (nmd/org-export-images)

  (re-search-forward subtree nil 't)
  (org-narrow-to-subtree)
  (nmd/export-context '(lambda () (org-latex-export-to-pdf nil 't))))

(defun nmd/org-export-images ()
  "Export images."

  (nmd/export-context
   '(lambda ()
      (save-restriction
        (widen)
        (save-excursion
          (goto-char (point-min))
          (let ((src-blocks (org-element-map
                                (org-element-parse-buffer)
                                'src-block
                              #'identity)))
            (setq src-blocks (nreverse src-blocks))
            (loop for src in src-blocks
                  do
                  (goto-char (org-element-property :begin src))
                  (let ((tangled-lang (org-element-property :language src)))
                    (if (and (bound-and-true-p tangled-lang)
                             (or (string= "ditaa" tangled-lang)
                                 (string= "dot" tangled-lang)
                                 (string= "plantuml" tangled-lang)
                                 (string= "R" tangled-lang)))
                        (org-babel-execute-src-block))))))))))

;;
;; FIXME: this.  It used to work in Org 7 or 8.
;; Not worth the time to fix now.
;;
;; (defun nmd/add-references (backend)
;;   (let*
;;       ((src-blocks
;;         (org-element-map
;;             (org-element-parse-buffer) 'src-block
;;           (lambda (src)
;;             (when (org-element-property :name src)
;;               src))))
;;        (refs
;;         (cl-loop
;;          for src in src-blocks collect
;;          (let* ((name (org-element-property :name src))
;;                 (references (string-join
;;                              (-remove
;;                               'null
;;                               (cl-loop
;;                                for osrc in src-blocks collect
;;                                (if (string-match
;;                                     (format "<<%s>>" name)
;;                                     (org-element-property :value osrc))
;;                                    (format "[[%s][%s]]"
;;                                            (org-element-property :name osrc)
;;                                            (org-element-property :name osrc))
;;                                  nil)))
;;                              ", ")))
;;            (message src)
;;            (if (not (string= "" references))
;;                (cons (org-element-property :end src)
;;                      (concat "/Referenced in " references "./\n\n"))
;;              nil)))))
;;     ;; for each block get the name and see if it is in any other block.
;;     (cl-loop for el in (reverse refs) do
;;              (when el
;;                (goto-char (car el))
;;                (insert (cdr el))))))

;; (add-hook 'org-export-before-parsing-hook 'nmd/add-references)
