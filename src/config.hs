import Propellor
import Propellor.Property.DiskImage
import Propellor.Property.Parted.Types
import qualified Propellor.Property.Apt as Apt
import qualified Propellor.Property.Chroot as Chroot
import qualified Propellor.Property.Cron as Cron
import qualified Propellor.Property.File as File
import qualified Propellor.Property.Git as Git
import qualified Propellor.Property.Grub as Grub
import qualified Propellor.Property.Hostname as Hostname
import qualified Propellor.Property.Network as Network
import qualified Propellor.Property.Sudo as Sudo
import qualified Propellor.Property.User as User


main :: IO ()
main = defaultMain hosts


-- The hosts propellor knows about.
hosts :: [Host]
hosts = [
  localhost
  ]


virtProps = props
  -- 1. run VMs, at all.
  -- 2. kernel mode HW virtualization.
  -- 3. view output of running system.
  & Apt.installed [ "virt-manager"  -- 1
    ,"libvirt-daemon-system"        -- 1
    ,"qemu-system-x86"              -- 2
    ,"virt-viewer"                  -- 3
    ,"gir1.2-spiceclientgtk3.0"     -- 3
    ]


localhost :: Host
localhost = host "localhost" $ props
  & osDebian (Stable "buster") X86_64
  -- change "props" to "virtProps" to install virtualization tools to
  -- run the image from this computer.
  & imageBuilt (RawDiskImage "/srv/cs790-p1.img") c MSDOS
    [ partition EXT4 `mountedAt` "/"
        `setFlag` BootFlag
    	`mountOpt` errorReadonly
        `addFreeSpace` MegaBytes 1024
    ]
  where
    c d = Chroot.debootstrapped mempty d $ experimentProps


experimentProps = props
  -- building a bootable system
  & osDebian (Stable "buster") X86_64
  & Cron.runPropellor (Cron.Times "30 * * * *")
  & Grub.installed Grub.PC

  -- building this system
  & Hostname.setTo "cs790-p1"

  -- hope the default network works.
  & Network.dhcp "ens3" `requires` Network.cleanInterfacesFile

  -- users
  & User.lockedPassword (User "root")
  & User.accountFor (User "science")
  & Sudo.enabledFor (User "science")
  & User "science" `User.hasInsecurePassword` "experiment"

  -- groups
  & User.systemGroup (Group "mumps")
  & User.systemGroup (Group "zookeeper")
  & User "science" `User.hasGroup` (Group "mumps")
  & User "science" `User.hasGroup` (Group "zookeeper")

  -- installed packages
  & Apt.stdSourcesList
  & Apt.unattendedUpgrades
  & Apt.installed [
    "ca-certificates"
    ,"coreutils"
    ,"ditaa"
    ,"elpa-dash"
    ,"elpa-graphviz-dot-mode"
    ,"elpa-org"
    ,"emacs"
    ,"etckeeper"
    ,"fis-gtm"
    ,"ghostscript"
    ,"git"
    ,"graphviz"
    ,"haveged"
    ,"initramfs-tools"
    ,"isc-dhcp-client"
    ,"latexmk"
    ,"linux-image-amd64"
    ,"make"
    ,"org-mode"
    ,"plantuml"
    ,"propellor"
    ,"pv"
    ,"python3"
    ,"r-cran-dplyr"
    ,"r-cran-ggplot2"
    ,"ssh"
    ,"sudo"
    ,"sysvinit-core"
    ,"texlive-fonts-recommended"
    ,"texlive-latex-base"
    ,"texlive-latex-extra"
    ,"texlive-latex-recommended"
    ,"texlive-plain-generic"
    ,"zookeeper"
    ]

  -- check out the repository
  & Git.cloned (User "science") "https://gitlab.com/nickdaly/cs790-p1.git" "/home/science/cs790-p1"
      (Just "remotes/origin/master")
  & Git.pulled (User "science") "https://gitlab.com/nickdaly/cs790-p1.git" "/home/science/cs790-p1"
      (Just "origin master")
  & Git.cloned (User "science") "https://gitlab.com/nickdaly/lorikeem.git" "/home/science/lorikeem"
      (Just "remotes/origin/master")
  & Git.pulled (User "science") "https://gitlab.com/nickdaly/lorikeem.git" "/home/science/lorikeem"
      (Just "origin master")

  -- basic configurations
  & "/home/science/.bashrc" `File.containsLine` "PATH='/usr/lib/x86_64-linux-gnu/fis-gtm/V6.3-007_x86_64/':$PATH"
  & "/home/science/.bashrc" `File.containsLine` "gtm_dist='/usr/lib/x86_64-linux-gnu/fis-gtm/V6.3-007_x86_64/'"
  & "/home/science/.bashrc" `File.containsLine` "needNewPassword=1"
  & "/home/science/.bashrc" `File.containsLine` "\
    \if [[ \"$needNewPassword\" == \"1\" ]]\n\
    \then\n\
    \    echo 'Please select a new password.'\n\
    \    passwd\n\
    \    sed -i 's/needNewPassword=1/needNewPassword=0/' /home/science/.bashrc\n\
    \fi"

  -- emacs configuration
  & File.dirExists "/home/science/.emacs.d/"
  & File.ownerGroup "/home/science/.emacs.d/" (User "science") (Group "science")
  & "/home/science/.emacs.d/init.el" `File.isSymlinkedTo` (File.LinkTarget "/home/science/cs790-p1/src/init.el")
