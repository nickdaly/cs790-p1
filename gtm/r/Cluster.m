Cluster
  ; just utility functions.
  q
  ; other convenience functions
GloToQuery(global)
  ; global must be $na(^RECORDS) or $na(EDITS)
  q $$FUNC^%LCASE($P(global,"^",2))
Get()
  q "get"
Set()
  q "set"
  ;
  ; supported cluster types
Zookeeper()
  q "Zookeeper"
  ;
query(clusterType,requestType,node,value,version)
  ; Get node values from any supported cluster.
  ; Unsupported cluster types will throw runtime errors.
  ; Optional: clusterType
  ; ==================================================================
  n cmd,output,exitStatus
  s:clusterType="" clusterType=^CLUSTER("type")
  ;
  ; build query (ref:cluster-build)
  x "s cmd=$$query"_clusterType_"(requestType,node,.value,.version)"
  s output=0
  ;
  ; run the query (ref:cluster-run)
  d runCmd(cmd,.output)
  ;
  ; parse query output (ref:cluster-parse)
  x "s exitStatus=$$parse"_clusterType_"(.output,.value,.version)"
  q exitStatus
  ;
queryZookeeper(requestType,nodeName,value,version)
  ; Get current variable counts from the Zookeeper cluster.
  ; - requestType is =$$Get= or =$$Set=
  ; - nodeName is =$$Records= or =$$Edits=
  ; Reference: value version
  ; ==================================================================
  n cmd
  s cmd="/usr/share/zookeeper/bin/zkCli.sh -server "_^CLUSTER("server")_" "
  s cmd=cmd_requestType_" /"_nodeName
  ;
  ; include value and version if specified (ref:value-specified)
  s cmd=cmd_$S((requestType=$$Set)!(requestType="create"):" "_value_" "_version,1:"")_" 2>&1; "
  ;
  ; echo exit status (ref:exit-status)
  s cmd=cmd_"echo ""$?"""
  q cmd
  ;
runCmd(cmd,output)
  ; Run a command, returning the exit status and line-delmited output.
  ; Reference: output
  ; ==================================================================
  n proc
  s proc="myproc"
  o proc:(command=cmd:readonly)::"PIPE"
  u proc
  f  r output(output):0 s:output(output)'="" %=$I(output) q:$zeof
  c proc
  u $p
  q
  ;
parseZookeeper(output,value,version)
  ; Returns cluster's current value, value's version, and exit status.
  ; Reference: value, version
  ; ==================================================================
  n line,firstValue,lineCount,exitStatus
  s firstValue=1
  ;
  f lineCount=0:1:output s line=output(lineCount) d
  . ; value immediately precedes cZxid
  . i ($P(line," ",1)="cZxid") s value=output(lineCount-1)
  . i $P(line," ",1)="dataVersion" s version=$P(line," ",3)
  s exitStatus=output(output-1)
  ;
  q exitStatus
  ;
increment(node,value,version)
  ; Set variable counts in the cluster.
  ; References: value, version
  ; ==================================================================
  n i,max,setSuccess,oldVer
  ;
  s max=10,value="",setSuccess="",oldVer=""
  ;
  ; try to set a new value in the cluster 10 times (ref:increment-retry)
  f i=1:1:max d  q:setSuccess
  . ;
  . ; only try to set data if we could successfully query the server.
  . i $$query("",$$Get,node,.value,.version)=0 d
  . . ;
  . . ; update state with the query response, specify a new value.
  . . s oldVer=version
  . . s value=value+1
  . . s %=$$query("",$$Set,node,value,.version)=0
  . . ;
  . . ; if the version has increased, we've set data. (ref:increment-success)
  . . s setSuccess=(oldVer<version)
  . ;
  . ; sleep for 0 - 0.5 seconds between tries (ref:increment-sleep)
  . h:'setSuccess $R(6)*0.1
  ;
  q setSuccess
  ;
nextId(typeGlo)
  ; Get the next ID of type, querying the cluster if necessary.
  ; ==================================================================
  n tried,id
  s tried=0
  ;
  ; pause here to make sure IDs are available.  (ref:wait-for-ids)
  f  q:'$$needQuery(typeGlo)  d
  . ; sleep for a moment if we've tried and failed to query.
  . h:(tried) $$timeToQuery(typeGlo)
  . ; only make the request if it's still required after sleeping.
  . j incrementJob(typeGlo)  ; (ref:job-off-query)
  . s tried=1
  ;
  ; don't check for ID availability, just lock.  it's astronomically
  ; unlikely that we'd burn through all =^CLUSTER("low-water")= values
  ; between here and when we checked, a statement ago.
  ;
  L +@typeGlo
  s id=$I(@typeGlo)
  L -@typeGlo
  q id
  ;
needQuery(typeGlo)
  ; Is the number of available records below the low-water mark?
  ; ==================================================================
  n minVals,recordsLeft
  ;
  s minVals=0,recordsLeft=0
  s minVals=^CLUSTER("low-water")
  s recordsLeft=@typeGlo@("max")-@typeGlo
  ;
  q recordsLeft<minVals
  ;
timeToQuery(typeGlo)
  ; How long should we wait before querying.
  ; May return negative seconds which are ignored by hang.
  ; ==================================================================
  n minTime,sinceLastRequest,waitTime
  ;
  s minTime=^CLUSTER("query-delay")
  s sinceLastRequest=0
  ; only compare against previous query if there has been one.
  ; otherwise, we'll wait unix-epoch seconds.
  s:@typeGlo@("query-time") sinceLastRequest=($$now^SetData("")-@typeGlo@("query-time"))
  s sinceLastRequest=minTime-sinceLastRequest
  ;
  q sinceLastRequest/1000000
  ;
incrementJob(typeGlo)
  ; Interpret the cluster's results to save new record IDs.
  ; ==================================================================
  n scalar,value,version
  ;
  ; lock the global immediately before continuing.
  L +@typeGlo:0
  ;
  ; quit if we couldn't lock the global or don't need to query anymore.
  q:'$T
  q:'$$needQuery(typeGlo)
  ;
  s scalar=^CLUSTER("scalar")
  s value=0,version=0
  ;
  ; update state, if we could reserve new IDs on the server.
  s @typeGlo@("query-time")=$$now^SetData("")
  i $$increment($$GloToQuery(typeGlo),.value,.version) d
  . s @typeGlo=value*scalar
  . s @typeGlo@("version")=version
  . s @typeGlo@("max")=(value+1)*scalar-1
  . j nameReservation($$GloToQuery(typeGlo),value)
  ;
  ; since increment can retry, make sure query-time is current.
  s @typeGlo@("query-time")=$$now^SetData("")
  ;
  ; unlock the global before quitting, too.
  L -@typeGlo
  ;
  q
  ;
nameReservation(node,value)
  ; Store that this node has reserved the ID range in the cluster.
  ; ==================================================================
  s %=$$query("","create",node_"/"_value,^CLUSTER("server"))
  q
  ;
writeChange(edit)
  ; Set a record edit in the cluster.
  ; edit must be some value in =$Q(^AUDIT)=.
  ; ==================================================================
  n auditValue,auditVersion,escaped,i
  ;
  s edit=edit_"="""_@edit_""""
  s escaped=""
  f i=1:1:$L(edit,"""") s escaped=escaped_$P(edit,"""",i)_"\"""
  s escaped=""""_$E(escaped,1,$L(escaped)-2)_""""
  s %=$$increment("audit",.auditValue,.auditVersion)
  s %=$$query("","create","audit/"_auditValue,escaped,"")
  ;
  q
  ;
readChangesWithLocks(since)
  ; Load database changes from the cluster.
  ; Assumes lock: =^CLUSTER("last-edit")=
  ; ==================================================================
  n value,version,edits
  ;
  s:'since since=^CLUSTER("last-edit")
  s %=$$increment("audit",.maxEdit,.version)
  ;
  ; piece out and load cluster data into the local server.
  f edits=since:1:maxEdit d
  . s value=0,version=0
  . s %=$$query("","get","audit/"_edits,.value,.version)
  . d:$E(value,1,7)="^AUDIT(" loadAudit(value)
  ;
  s ^CLUSTER("last-edit")=maxEdit
  ;
  q
  ;
loadAudit(value)
  ; Loads cluster edits into server globals.
  ; Sets the first subnode to the current Unix epoch, to indicate when
  ; the server was notified that the edit arrived.
  ; ==================================================================
  n params
  ;
  ; update local audit time
  s params=$P(value,"(",2)
  s $P(params,",",1)=$$now^SetData("")  ; (ref:update-local-audit)
  s $P(value,"(",2)=params
  ;
  ; replay ^AUDIT changes.
  s @value
  ;
  ; I believe this is only possible because the ^AUDIT nodes may
  ; never contain commas.
  n numNodes
  s numNodes=$L($R,",")
  i numNodes=6 x "d loadEntry("_$TR($P(value,"(",2),")=",",")_")"
  i numNodes=7 x "d loadMultiEntry("_$TR($P(value,"(",2),")=",",")_")"
  ;
  q
  ;
loadEntry(localInstant,editInstant,global,recordId,editId,field,value)
  ; Loads cluster edits into non-list server globals.
  ; Ignores: localInstant
  ; ==================================================================
  x "s ^"_global_"(recordId,editId,field,editInstant)=value"
  q
  ;
loadMultiEntry(localInstant,editInstant,global,recordId,editId,field,line,value)
  ; Loads cluster edits into list-type server globals (=^PatientLink=).
  ; Ignores: localInstant
  ; ==================================================================
  x "s ^"_global_"(recordId,editId,field,editInstant,line)=value"
  x "s %=$I(^"_global_"(recordId,editId,field))"
  q
  ;
readChanges(since)
  ; Load database changes from the cluster.
  ; ==================================================================
  L +^CLUSTER("last-edit")
  d readChangesWithLocks(since)
  L -^CLUSTER("last-edit")
  q
  ;
readChangesJob()
  ; Continuously read edits from the cluster.
  ; ==================================================================
  f  q:^CLUSTER("stop")  h ^CLUSTER("load-delay") d readChanges("")
  q
  ;
setup()
  ; Perform one-time cluster setup.
  ; ==================================================================
  L +^CLUSTER:0
  q:'$T
  s $ETRAP="B"
  s ^CLUSTER("last-edit")=0
  s ^CLUSTER("load-delay")=60*1000*1000    ; 1m
  s ^CLUSTER("query-delay")=0.1*1000*1000  ; 0.1s  (ref:query-delay)
  s ^CLUSTER("server")="localhost:2181"
  s ^CLUSTER("type")=$$Zookeeper
  s ^CLUSTER("scalar")=$$loadScalar()
  s ^CLUSTER("stop")=0
  s ^CLUSTER("low-water")=^CLUSTER("scalar")*0.05  ; (ref:low-water)
  L -^CLUSTER
  q
  ;
loadScalar()
  ; Load the cluster's scalar value.
  ; ==================================================================
  s ^CLUSTER("low-water")=500
  s %=$$query("","get","scalar",.value,.version)
  n value,version
  s:'value value=10000  ; (ref:cluster-load-scalar)
  q value
  ;
start()
  ; Query the cluster for updates on occasion.
  ; ==================================================================
  s ^CLUSTER("stop")=0
  j readChangesJob()
  q
  ;
stop()
  ; Stop querying the cluster.
  ; ==================================================================
  s ^CLUSTER("stop")=1
  q
  ;
