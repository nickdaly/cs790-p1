SetData
  ; just utility functions.
  q
now(time)
  ; Returns the current Unix epoch time or the passed in time.
  ; ==================================================================
  q:time time
  q $ZUT
  ;
strip(string)
  ; Remove whitespace from ends of string.
  ; ==================================================================
  n whitespace,start,end
  ;
  s whitespace=" "_$C(9,10,13)
  ;
  f start=1:1:$L(string) q:'(whitespace[start)  ; (ref:whitespace)
  f end=$L(string):-1:1 q:'(whitespace[end)
  ;
  q $E(string,start,end)  ; (ref:extract)
  ;
  ; ==================================================================
  ; Convenenice functions to refer to owning globals.
Records()
  q $name(^RECORDS)
Edits()
  q $name(^EDITS)
Med()
  q $name(^MEDICATION)
MedRx()
  q $name(^MEDRX)
Pat()
  q $name(^PATIENT)
PatLink()
  q $name(^PATIENTLINK)
Proc()
  q $name(^PROCEDURE)
ProcRx()
  q $name(^PROCRX)
  ;
isGoodGlobal(global)
  ; Is the global a well-formed data global?
  ; ==================================================================
  q:global="" 0          ; (ref:bg-null)
  q:$E(global,1)'="^" 0  ; (ref:bg-global)
  q:'$D(global) 0        ; (ref:bg-exist)
  q:(global["^AUDIT") 0  ; (ref:bg-audit)
  q 1
  ;
NewRecord(global)
  ; Reserve a new record ID.
  ; ==================================================================
  n editInstant,recordId
  ;
  q:'$$isGoodGlobal(global) 0
  ;
  s editInstant=$$now("")
  s recordId=$$nextId^Cluster($$Records)  ; (ref:NewRecord-create)
  ;                                         (ref:NewRecord-audit)
  s ^AUDIT(editInstant,editInstant,$TR(global,"^",""))=recordId
  ;
  q recordId                              ; (ref:NewRecord-return)
  ;
NewEdit(global,recordId,editInstant)
  ; Create a new edit in a record.
  ; Optional: editInstant
  ; ==================================================================
  q:'$$isGoodGlobal(global) 0
  ;
  s:editInstant="" editInstant=$$now("")  ; (ref:null-instant)
  s editId=$$nextId^Cluster($$Edits)
  ;                                         (ref:audit-record)
  s ^AUDIT(editInstant,editInstant,$TR(global,"^",""),recordId)=editId
  ;
  q editId
  ;
SetRecord(global,recordId,editId,editInstant,field,value)
  ; Set data in a record.
  ; Optional: editInstant, value
  ; ==================================================================
  n newNode
  ;
  q:'$$isGoodGlobal(global) 0
  q:(+recordId=0)!(+editId=0)!(field=0) 0
  ;
  s:'editInstant editInstant=$$now("")
  ;
  ; Avoid overlapping data entries (ref:find-free-instant)
  f editInstant=editInstant:1 d  q:newNode
  . ; lock the potential edit
  . L +@global@(recordId,editId,field,editInstant):0
  . ; if the edit could be locked, check to see if it exists.
  . ; if it couldn't be locked, somebody else is already using it
  . s:$T newNode='$D(@global@(recordId,editId,field,editInstant))
  . ; if the node doesn't yet exist, use it.
  . q:newNode
  . ; if the node already exists, unlock it and try the next.
  . L -@global@(recordId,editId,field,editInstant)
  ;
  s @global@(recordId,editId,field,editInstant)=value
  L- @global@(recordId,editId,field,editInstant)
  s ^AUDIT(editInstant,editInstant,$TR(global,"^",""),recordId,editId,field)=value
  ;
  j writeChange^Cluster($R)  ; (ref:cluster-submit)
  ;
  q value
  ;
AppendRecord(global,recordId,editId,editInstant,field,value)
  ; Add data to the end of a list in a record.
  ; Optional: editInstant, value
  ; ==================================================================
  n entry
  q:'$$isGoodGlobal(global) 0
  q:(+recordId=0)!(+editId=0)!(field=0) 0
  ;
  s:'editInstant editInstant=$$now("")
  s entry=$I(@global@(recordId,editId,field))
  ;
  s @global@(recordId,editId,field,editInstant,entry)=value
  s ^AUDIT(editInstant,editInstant,$TR(global,"^",""),recordId,editId,field,entry)=value
  ;
  j writeChange^Cluster($R)
  ;
  q value
  ;
GetRecordList(global,recordId,editId,editInstant,field,result)
  ; Get data from a record list, returned in the result array.
  ; Optional: editInstant
  ; Reference: result
  ; ==================================================================
  q:'$$isGoodGlobal(global) 0
  q:(+recordId=0)!(+editId=0)!(field=0) 0
  ;
  s:'editInstant editInstant=$$now()
  m result=@global@(recordId,editId,field,editInstant)  ; (ref:get-merge-array)
  ;
  q result
  ;
SetRecordList(global,recordId,editId,editInstant,field,newData)
  ; Set data in a record list.
  ; Optional: editInstant
  ; Reference: newData
  ; ==================================================================
  q:'$$isGoodGlobal(global) 0
  q:(+recordId=0)!(+editId=0)!(field=0) 0
  ;
  s:'editInstant editInstant=$$now()
  m @global@(recordId,editId,field,editInstant)=newData  ; (ref:set-merge-array)
  ;
  q newData
  ;
killRecord(global,recordId)
  ; Erase a record from the database.
  ; The ID will not be reused later.
  ; ==================================================================
  q:'$$isGoodGlobal(global) 0
  q:(+recordId=0) 0
  ;
  s editInstant=$$now()
  s ^AUDIT(editInstant,editInstant,$TR(global,"^",""))="K "_recordId
  ;
  k @global@(recordId)
  ;
  q editInstant
  ;
setup()
  ; Perform one-time database setup.
  ; Create all the nodes needed for cluster synchronization.
  ; ==================================================================
  s ^RECORDS=0
  s ^RECORDS("max")=0
  s ^RECORDS("query-time")=0
  s ^RECORDS("version")=0
  ;
  s ^EDITS=0
  s ^EDITS("max")=0
  s ^EDITS("query-time")=0
  s ^EDITS("version")=0
  ;
  q
  ;
