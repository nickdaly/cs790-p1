TestData
  ; utility functions
  q
setup()
  n medRecs,procRecs
  s $ETRAP="B"
  d MakeData(.medRecs,.procRecs)
  d Patient1(.medRecs,.procRecs)
  q
MakeData(medRecs,procRecs)
  ; Create new medication and procedure records.
  ; Reference: medRecs, procRecs
  ; ==================================================================
  s %=$$MakeRecNames($$Med^SetData,"acetaminophen,albuterol,ibuprofen,loratidine",.medRecs)
  s %=$$MakeRecNames($$Proc^SetData,"cbc,chest x-ray,lipid panel,rapid strep test",.procRecs)
  q
  ;
MakeRecNames(global,recNames,recIds)
  ; Record creation helper function.
  ; Reference: recIds
  ; ==================================================================
  n recId,recEdit,i
  ;
  ; iterate over the name list
  f i=1:1:$L(recNames,",") d
  . ;
  . ; reserve new record and edit IDs (ref:rec-new-ids)
  . s recId=$$NewRecord^SetData(global)
  . s recEdit=$$NewEdit^SetData(global,recId,"")
  . ;
  . ; save those IDs in the result arrays (ref:rec-store-ids)
  . s recIds(recId)=recEdit
  . ;
  . ; actually save the record's name data. (ref:rec-save-name)
  . s %=$$SetRecord^SetData(global,recId,recEdit,"",1,$$strip^SetData($P(recNames,",",i)))
  ;
  q 1
  ;
Patient1(medRecs,procRecs)
  ; Create the first patient and their link record.
  ; Reference: medRecs, procRecs
  ; ==================================================================
  n patId,patEdit
  ;
  ; create a patient (ref:new-pat1)
  s patId=$$NewRecord^SetData($$Pat^SetData)
  s patEdit=$$NewEdit^SetData($$Pat^SetData,patId,"")
  ;
  ; create a patientLink record (ref:new-link1)
  s patLinkId=$$NewRecord^SetData($$PatLink^SetData)
  s patLinkEdit=$$NewEdit^SetData($$PatLink^SetData,patLinkId,"")
  ;
  ; link patient to their link record (ref:make-link1)
  s %=$$SetRecord^SetData($$Pat^SetData,patId,patEdit,"",1,patLinkId)
  ;
  ; store demographics (ref:new-demographics1)
  d Patient1Roomed(patId,patEdit)
  ;
  ; create orders (ref:new-orders1)
  d Patient1Orders(patId,patEdit,patLinkId,patLinkEdit,$O(medRecs("")),$O(procRecs("")))
  ;
  q
  ;
Patient1Roomed(patId,patEdit)
  ; Fill in patient 1 vitals
  ; ==================================================================
  n vitals
  ;
  ; basic demographics (ref:demogs)
  ; 2. Name
  s %=$$SetRecord^SetData($$Pat^SetData,patId,patEdit,"",2,"zzztest zzztest")
  ; 3. Birthdate: 35 years ago
  s %=$$SetRecord^SetData($$Pat^SetData,patId,patEdit,"",3,$H-(365.25*35/1))
  ; 4. Sex: Female
  s %=$$SetRecord^SetData($$Pat^SetData,patId,patEdit,"",4,"female")
  ;
  ; Vitals (ref:vitals-list)
  s vitals(5)=+$H              ; today's appointment:
  s vitals(6)=120,vitals(7)=80 ; bp: 120/80
  s vitals(8)=20               ; respiration: 20
  s vitals(9)=37               ; temperature: 37
  ;
  ; Iteratively store the vitals (ref:vitals-iter)
  f i=$O(vitals("")):1:$O(vitals("",-1)) d
  . s %=$$SetRecord^SetData($$Pat^SetData,patId,patEdit,"",i,vitals(i))
  ;
  q
  ;
Patient1Orders(patId,patEdit,patLinkId,patLinkEdit,medId,procId)
  ; Create patient 1 orders
  ; Reference: medRxId, medRxEditId, procRxId, procRxEditId
  ; ==================================================================
  n med,proc,day,us
  ;
  s day=24*60*60,us=1000000 ; seconds per day, microseconds per second
  ;
  ; new prescriptions (ref:new-rx)
  s medRxId=$$NewRecord^SetData($$MedRx^SetData)
  s medRxEditId=$$NewEdit^SetData($$MedRx^SetData,medRxId,"")
  s procRxId=$$NewRecord^SetData($$ProcRx^SetData)
  s procRxEditId=$$NewEdit^SetData($$ProcRx^SetData,procRxId,"")
  ;
  ; Medication
  s med(1)=patId
  s med(2)=medId
  s med(3)=1577880000*us  ; start: noon on January 1st, 2020, GMT
  s med(4)=1580299200*us  ; stop: noon on January 29th, 2020, GMT
  s med(5)=day*us         ; take once daily
  s med(6)=28             ; dispense 28 pills
  s med(7)=(7*day*4)*us   ; refill every 4 weeks
  s med(8)=0              ; no refills remaining
  f i=1:1:8 s %=$$SetRecord^SetData($$MedRx^SetData,medRxId,medRxEditId,"",i,med(i))
  ;
  ; Procedure details
  s proc(1)=patId
  s proc(2)=procId
  s proc(3)=1577887500*us  ; scheduled: 2:05 PM on January 15th
  s proc(4)="ASAP"         ; comment
  f i=1:1:4 s %=$$SetRecord^SetData($$ProcRx^SetData,procRxId,procRxEditId,"",i,proc(i))
  ;
  ; link to link-record
  s %=$$AppendRecord^SetData($$PatLink^SetData,patLinkId,patLinkEdit,"",2,medRxId)
  s %=$$AppendRecord^SetData($$PatLink^SetData,patLinkId,patLinkEdit,"",3,procRxId)
  ;
  q
  ;
