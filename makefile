.PHONY: all clean data

all: html paper src

clean:
	-rm -r bin doc/img/* gtm
	-rm notes.html
	-rm notes.pdf

diskimage: src/config.hs makefile
	mkdir -p bin
	-echo "A" | propellor --init
	-mv ~/.propellor/config.hs ~/.propellor/config.hs.bak
	cp src/config.hs ~/.propellor/config.hs
	propellor --spin localhost
	-mv ~/.propellor/config.hs.bak ~/.propellor/config.hs
	-xz -c /srv/cs790-p1.img | pv > bin/cs790-p1.img.xz

# these should run successfully from within the generated disk image.
html: notes.org makefile doc
	emacs --batch -l src/init.el --visit notes.org --eval "(nmd/org-html-export-to-html-non-interactive)"

paper: notes.org makefile doc
	emacs --batch -l src/init.el --visit notes.org --eval "(nmd/org-export-subtree-to-pdf \"e791271b-f0d7-4196-a9f4-eb68517de79f\")"

doc: doc/img
	mkdir -p doc

doc/img:
	mkdir -p doc/img

src: src/new-env
	src/new-env

src/new-env: notes.org makefile gtm/r
	emacs notes.org --batch -f org-babel-tangle

gtm/r:
	mkdir -p gtm/r

data:
	mkdir -p data
	src/perf-test | tee data/cluster-sets-`date -u +%F_%T`.log
