# Lock-free Mumps

This project tries to make Mumps a more distributed database by allowing multiple users to write data at the same time, and then integrating that new data before users finalize their decisions.  This integration is limited by communication: if a node is down, other nodes can't be aware of those un-communicated changes.

# Review

This repository is built into three files, the [research paper](https://nickdaly.gitlab.io/cs790-p1/notes.pdf), the [disk image](https://gitlab.com/nickdaly/cs790-p1/-/blob/master/bin/cs790-p1.img.xz) to reproduce the experiment, the raw [experimental notes](https://nickdaly.gitlab.io/cs790-p1/).

# Use

The quickest way to play with the experiment is probably to download the disk image and launch it in a VM.  From that VM, login as science:experiment.  It'll make you change your password the first time you login.  Then, you can:

    cd cs790-p1
    make all

You can then examine or run the source in the `gtm` directory, and check out the exported paper and notes files in `notes.pdf` or `notes.html`, respectively.

To test the source, run:

    ./gtm/gtm-env
    d ^test
    h

If all goes well, it should mention how it incremented the `XTEST` variable.

# Requirements

The fastest way to get started is to copy the disk image to a USB stick and run that or install [Propellor](https://propellor.branchable.com/) from your package manager and run Propellor for the `src/config.hs` file.

# License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

The disk image's contents (`bin/cs790-p1.img.xz*`) are available under their respective licenses as noted in the `/usr/share/doc/$PACKAGE-NAME/copyright` file, but are all available from the Debian [main archive area](https://www.debian.org/doc/debian-policy/ch-archive#s-main) and are thus freely redistributable.
